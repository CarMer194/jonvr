using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutScene3 : MonoBehaviour
{
    // Player 
    public GameObject player;

    // Triggers 
    public GameObject triggerCutScene1;
    public GameObject triggerCutScene1a;
    public GameObject triggerCutScene1b;
    public GameObject triggerCutScene2;
    public GameObject triggerCutScene2b;
    public GameObject triggerCutScene2c;
    public GameObject triggerCutScene2d;
    public GameObject triggerCutSceneEnd;

    // define the audio clips
    public AudioClip exploringFun;
    public AudioClip teachersOffice;

    // define audioSources
    private AudioSource audioExploringFun;
    private AudioSource audioTeachersOffice;

    private void Awake()
    {
        if (GameMode.isFreeMode)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // disabling other triggers
        triggerCutScene1.SetActive(false);
        triggerCutScene1a.SetActive(false);
        triggerCutScene1b.SetActive(false);
        triggerCutScene2.SetActive(false);
        triggerCutScene2b.SetActive(false);
        triggerCutScene2c.SetActive(false);
        triggerCutScene2d.SetActive(false);
        triggerCutSceneEnd.SetActive(false);

        // adding the audioSources
        audioExploringFun = AddAudio(exploringFun, false, true, 1);
        audioTeachersOffice = AddAudio(teachersOffice, false, true, 1);

        if (other.CompareTag("Player"))
        {
            StartCoroutine(TheSequence());
        }

    }

    IEnumerator TheSequence()
    {
        player.GetComponent<Animation>().Play("CutScene3");
        audioExploringFun.Play();
        yield return new WaitForSeconds(22);
        audioTeachersOffice.Play();
        yield return new WaitForSeconds(33);

        triggerCutSceneEnd.SetActive(true);
        this.gameObject.SetActive(false);
    }

    // function to add audio sources
    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {

        AudioSource newAudio = gameObject.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;

    }
}
