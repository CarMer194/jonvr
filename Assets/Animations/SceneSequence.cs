using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSequence : MonoBehaviour
{
    public CutsceneManager manager;

    private void OnTriggerEnter(Collider other)
    {
        manager.Play("newMainCutScene");
    }
}
