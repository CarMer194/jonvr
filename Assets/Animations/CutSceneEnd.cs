using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneEnd : MonoBehaviour
{
    // Player 
    public GameObject player;

    // Medal
    public GameObject medal;

    // Triggers
    public GameObject triggerCutScene1;
    public GameObject triggerCutScene1a;
    public GameObject triggerCutScene1b;
    public GameObject triggerCutScene2;
    public GameObject triggerCutScene2b;
    public GameObject triggerCutScene2c;
    public GameObject triggerCutScene2d;
    public GameObject triggerCutScene3;

    // define the audio clips
    public AudioClip tourFinished;
    public AudioClip reasonOfTour;

    // define audioSources
    private AudioSource audioTourFinished;
    private AudioSource audioReasonOfTour;

    private void Awake()
    {
        if (GameMode.isFreeMode)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // disabling other triggers
        triggerCutScene1.SetActive(false);
        triggerCutScene1a.SetActive(false);
        triggerCutScene1b.SetActive(false);
        triggerCutScene2.SetActive(false);
        triggerCutScene2b.SetActive(false);
        triggerCutScene2c.SetActive(false);
        triggerCutScene2d.SetActive(false);
        triggerCutScene3.SetActive(false);

        // adding the audioSources
        audioTourFinished = AddAudio(tourFinished, false, true, 1);
        audioReasonOfTour = AddAudio(reasonOfTour, false, true, 1);

        if (other.CompareTag("Player"))
        {
            StartCoroutine(TheSequence());
        }

    }

    IEnumerator TheSequence()
    {
        player.GetComponent<Animation>().Play("CutSceneEnd");
        yield return new WaitForSeconds(5);
        audioTourFinished.Play();
        yield return new WaitForSeconds(5);
        medal.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(5);
        audioReasonOfTour.Play();
        yield return new WaitForSeconds(20);

        this.gameObject.SetActive(false);
    }

    // function to add audio sources
    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {

        AudioSource newAudio = gameObject.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;

    }
}
