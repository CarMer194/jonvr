using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutScene2 : MonoBehaviour
{
    // player 
    public GameObject player;

    // Medal
    public GameObject medal;

    // triggers
    public GameObject triggerCutScene1;
    public GameObject triggerCutScene1a;
    public GameObject triggerCutScene1b;
    public GameObject triggerCutScene2b;
    public GameObject triggerCutScene2c;
    public GameObject triggerCutScene2d;
    public GameObject triggerCutScene3;
    public GameObject triggerCutSceneEnd;

    // define the audio clips
    public AudioClip welcomeSecondLevel;
    public AudioClip centralAreaA;

    // define audioSources
    private AudioSource audioWelcomeSecondLevel;
    private AudioSource audioCentralAreaA;

    private void Awake()
    {
        if (GameMode.isFreeMode)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // disabling other triggers
        triggerCutScene1.SetActive(false);
        triggerCutScene1a.SetActive(false);
        triggerCutScene1b.SetActive(false);
        triggerCutScene2b.SetActive(false);
        triggerCutScene2c.SetActive(false);
        triggerCutScene2d.SetActive(false);
        triggerCutScene3.SetActive(false);
        triggerCutSceneEnd.SetActive(false);

        // adding the audioSources
        audioWelcomeSecondLevel = AddAudio(welcomeSecondLevel, false, true, 1);
        audioCentralAreaA = AddAudio(centralAreaA, false, true, 1);

        if (other.CompareTag("Player"))
        {
            StartCoroutine(TheSequence());
        }

    }

    IEnumerator TheSequence()
    {
        player.GetComponent<Animation>().Play("CutScene2");
        yield return new WaitForSeconds(16);
        audioWelcomeSecondLevel.Play();
        yield return new WaitForSeconds(5);
        medal.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(11);
        audioCentralAreaA.Play();
        yield return new WaitForSeconds(7);

        triggerCutScene2b.SetActive(true);
        this.gameObject.SetActive(false);
    }

    // function to add audio sources
    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {

        AudioSource newAudio = gameObject.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;

    }
}
