using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class RadioPlayer : MonoBehaviour
{

    public bool isPlaying = false;
    public XRGrabInteractable grabInteractor;
    public AudioSource audioSource;
    void OnEnable()
    {
        grabInteractor.selectEntered.AddListener(PlayRadio);
    }

    void OnDisable()
    {
        grabInteractor.selectEntered.RemoveListener(PlayRadio);
    }

    private void PlayRadio(SelectEnterEventArgs arg0)
    {
        Debug.Log("Hola");
        if(isPlaying)
        {
            isPlaying = !isPlaying;
            audioSource.Stop();
        } else
        {
            isPlaying = true;
            audioSource.Play(0);
        }
    }
}
