using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Labo5Scene : MonoBehaviour
{
    public Light luzProyector;
    public VideoPlayer videoPlayer;
    
    int counter = 0;    
    bool played = false;

    IEnumerator TheSequence()
    {   
        while(counter < 10)
        {            
            luzProyector.intensity += 1;
            counter += 1;
            yield return new WaitForSeconds(1);
        }
        

        videoPlayer.Play();
    }

    private void OnTriggerEnter(Collider other)
    {        
        if (!played)
        {            
            played = true;
            StartCoroutine(TheSequence());
        }
            
    }
}
