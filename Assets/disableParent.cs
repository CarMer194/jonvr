using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableParent : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform gameobject1;
    public CutsceneManager sceneManager;

    private void OnTriggerExit(Collider other)
    {
        if (!sceneManager.isScenePlaying)
        {
            gameobject1.position = new Vector3(-100, -100, 0);
        }
    }
}
