using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[Serializable]
public class Cutscene2 : CutsceneType
{
    // define the audio clips
    public AudioClip welcomeSecondLevel;
    public AudioClip centralAreaA;

    // define audioSources
    private AudioSource audioWelcomeSecondLevel;
    private AudioSource audioCentralAreaA;

    // Medal
    public GameObject medal;

    public override IEnumerator Play()
    {
        audioWelcomeSecondLevel = ConfigureAudioSource(welcomeSecondLevel, false, true, 1);
        audioCentralAreaA = ConfigureAudioSource(centralAreaA, false, true, 1);

        manager.isScenePlaying = true;

        yield return new WaitForSeconds(16);
        audioWelcomeSecondLevel.Play();
        yield return new WaitForSeconds(5);
        medal.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(11);
        audioCentralAreaA.Play();
        yield return new WaitForSeconds(7);

        manager.isScenePlaying = false;
    }

    public override AudioSource ConfigureAudioSource(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = player.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;
    }
}