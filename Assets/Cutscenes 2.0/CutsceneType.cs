using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CutsceneType: MonoBehaviour 
{
    public string title;
    public GameObject trigger;
    public float[] delays;
    public float finalDelay;
    public AudioClip[] audioClips;
    public AudioSource source;
    public GameObject cutsceneZone;
    public CutsceneType nextCutscene;
    public GameObject player;
    public CutsceneManager manager;

    public virtual IEnumerator Play() {
        Debug.Log("base play method");
        yield return new WaitForSeconds(0);
    }

    public virtual AudioSource ConfigureAudioSource(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = player.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;
        return newAudio;
    }

    public void OnDisable()
    {
        title = null;
        trigger = null;
        delays = null;
        finalDelay = 0.0f;
        audioClips = null;
        source = null;
        cutsceneZone = null;
        nextCutscene = null;
        player = null;
    }
}
