using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[Serializable]
public class Cutscene: CutsceneType
{
    public override IEnumerator Play()
    {
        manager.isScenePlaying = true;
        var cutscenes = audioClips.Zip(delays, (audioClip, delay) => new { Clip = audioClip, Delay = delay});
        foreach(var cutscene in cutscenes)
        {
            source = ConfigureAudioSource(cutscene.Clip, false, true, 1);
            yield return new WaitForSeconds(cutscene.Delay);
            source.Play();
        }
        yield return new WaitForSeconds(finalDelay);
        manager.isScenePlaying = false;
        //this.gameObject.SetActive(false);
    }

    public override AudioSource ConfigureAudioSource(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = player.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;
    }
}