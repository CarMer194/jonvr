using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    [SerializeField]
    public CutsceneType[] cutscenes;
    public AudioSource source;
    public GameObject player;
    public bool isScenePlaying;
    void Awake()
    {
        if (GameMode.isFreeMode) {
            foreach (CutsceneType cutscene in cutscenes) {
                cutscene.gameObject.SetActive(false);
            }
        }
    }

    public void Play(string name)
    {
        string name2 = name;
        var cutsceneToPlay = cutscenes.First(cutscene => cutscene.title.Equals(name));    
        cutsceneToPlay.source = source;
        player.GetComponent<Animation>().Play(name);
        //cutsceneToPlay.nextCutscene = GetNextCutscene(cutsceneToPlay);
        cutsceneToPlay.player = player;
        StartCoroutine(cutsceneToPlay.Play());
    }

    /*CutsceneType GetNextCutscene(CutsceneType cutscene)
    {
        var nextIndex = Array.IndexOf(cutscenes, cutscene) + 1;
        if (nextIndex < cutscenes.Length)
        {
            var nextCutscene = cutscenes[nextIndex];
            return nextCutscene;
        }
        return null;
    }*/
}