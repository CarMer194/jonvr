using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableWithParent : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        this.gameObject.SetActive(this.gameObject.activeSelf);
    }
}
