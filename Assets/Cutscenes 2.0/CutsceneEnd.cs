using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[Serializable]
public class CutsceneEnd : CutsceneType
{
    // define the audio clips
    public AudioClip tourFinished;
    public AudioClip reasonOfTour;

    // define audioSources
    private AudioSource audioTourFinished;
    private AudioSource audioReasonOfTour;

    // Medal
    public GameObject medal;

    public override IEnumerator Play()
    {
        audioTourFinished = ConfigureAudioSource(tourFinished, false, true, 1);
        audioReasonOfTour = ConfigureAudioSource(reasonOfTour, false, true, 1);

        manager.isScenePlaying = true;

        yield return new WaitForSeconds(5);
        audioTourFinished.Play();
        yield return new WaitForSeconds(5);
        medal.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(5);
        audioReasonOfTour.Play();
        yield return new WaitForSeconds(20);

        manager.isScenePlaying = false;
    }

    public override AudioSource ConfigureAudioSource(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = player.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;

        return newAudio;
    }
}
