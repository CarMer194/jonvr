using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartWithoutGuide : MonoBehaviour
{
    public void Play()
    {
        GameMode.isFreeMode = true;
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
