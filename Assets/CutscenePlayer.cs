using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CutscenePlayer : MonoBehaviour
{

    public XRGrabInteractable grabInteractor;
    public new string name;
    public CutsceneManager manager;
    bool didFinishRemoving = false;
    bool isInside = false;

    void OnEnable()
    {
        grabInteractor.selectEntered.AddListener(PlayCutscene);
    }

    void OnDisable()
    {
        grabInteractor.selectEntered.RemoveListener(PlayCutscene);
    }

    void Update()
    {
        if(isInside) Debug.Log("Active");
    }
    private void PlayCutscene(SelectEnterEventArgs arg0)
    {
        string name2 = name;
        if (!manager.isScenePlaying && isInside)
        {
            manager.Play(name);
            isInside = false;
            Debug.Log("Inside");
        }
    }

    void OnTriggerEnter(Collider object2)
    {
        isInside = true;
        Debug.Log("Enter Trigger");
    }

    void OnTriggerExit(Collider object2)
    {
        isInside = false;
        Debug.Log("Exit Trigger");
    }
}
