using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToPositonScript : MonoBehaviour
{
    public Transform objetivo;
    Rigidbody rb;
    // Start is called before the first frame update
    private void OnTriggerExit(Collider other)
    {
        rb.MovePosition(objetivo.transform.position);
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

}
